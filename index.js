//需要配合babel才能这么写
// let components = require.context('./components', true, /\.js$/)
// let cowb = {}
// components.keys().forEach((fileName) => {
//     Object.values(components(fileName).default).forEach(r => Object.assign(cowb, r))
// })

import * as array from './modules/array'
import * as util from './modules/util'
import * as bom from './modules/bom'
import * as antdv from './modules/antdv'
import * as cache from './modules/cache'
import * as string from './modules/string'
import * as mock from './modules/mock'
import * as object from './modules/object'
import * as time from './modules/time'
import * as functions from './modules/function'
import * as math from './modules/math'

let _ = {
    ...math,
    ...array,
    ...time,
    ...util,
    ...object,
    ...bom,
    ...antdv,
    ...cache,
    ...string,
    ...mock,
    ...functions
}

export default _